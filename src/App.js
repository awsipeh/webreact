import React from 'react';
import {
  Navbar,
  Nav,
  Button,
  NavDropdown,
  Form,
  FormControl,
  div,
  Image,
  Badge,
  Card,
  Container,
  Row,
  Col

} from 'react-bootstrap';
import gambar from './wardah1.png' ;

function Header(){
  return(
    <div style={{backgroundColor:"#5bc5c5"}}>
    <Navbar expand="lg">
    <img
            alt=""
            src="https://www.wardahbeauty.com/assets/images/logo.png"
            width="200"
            height="75"
            className="d-inline-block align-top"
          />
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <NavDropdown title="PRODUK" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
          </NavDropdown>
          <Nav.Link href="#home">ARTICLES</Nav.Link>
          <NavDropdown title="VIRTUAL MAKE UP" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
          </NavDropdown>
          <Nav.Link href="#home">SKILL ANALYZER</Nav.Link>
          <NavDropdown title="ACTIVITIES" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
          </NavDropdown>

        </Nav>
        <Form inline bg="light">
           <Nav.Link href="#home">MASUK</Nav.Link>
            <NavDropdown title="ID" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
          </NavDropdown>
               <img
                alt=""
                src="https://cdn2.iconfinder.com/data/icons/font-awesome/1792/search-512.png"
                width="25"
                height="25"
                className="d-inline-block align-top"
              />
        </Form>
      </Navbar.Collapse>
    </Navbar>
</div>
);
}

function Banner(){
  return(
      <Image src="https://www.wardahbeauty.com/medias/sliders/slider-mobile-1610352723.jpg" fluid />
    );
}


function Bannerdua(){
  return(
    <div style={{backgroundImage:"url('https://www.wardahbeauty.com/assets/images/manifesto-bg-desktop.jpg')",paddingTop:"10px",height:"auto"}}>
      <Image src="https://www.wardahbeauty.com/assets/images/manifesto-header-image-1573717060.png" style={{display:"block",marginLeft:"auto",marginRight:"auto",marginTop:"25px",marginBottom:"50px"}}/>
    <Container>
      <Row>
        <Col>
          <Image src="https://www.wardahbeauty.com/medias/manifesto/manifesto-1573150593.png" style={{width:"175px",height:"175px",display:"block",marginLeft:"auto",marginRight:"auto"}}/>
          <h2 style={{fontSize:"23px",textAlign:"center",color:"#5bc5c5"}}>Murni & Aman</h2>
          <p style={{fontSize:"15px",textAlign:"center",color:"#5bc5c5"}}>Kemurnian alam dipadukan dengan proses yang halal, modern, dan terpercaya akan menghasilkan produk halal berkualitas tinggi.</p>
        </Col>
        <Col>
          <Image src="https://www.wardahbeauty.com/medias/manifesto/manifesto-1573150897.png" style={{width:"175px",height:"175px",display:"block",marginLeft:"auto",marginRight:"auto"}}/>
          <h2 style={{fontSize:"23px",textAlign:"center",color:"#5bc5c5"}}>Innovation</h2>
          <p style={{fontSize:"15px",textAlign:"center",color:"#5bc5c5"}}>Sentuhan inovasi bertaraf internasional yang sesuai dengan tren global dilakukan untuk menghasilkan produk dengan standar mutu tinggi.</p>
        </Col>
        <Col>
          <Image src="https://www.wardahbeauty.com/medias/manifesto/manifesto-1573151386.png" style={{width:"175px",height:"175px",display:"block",marginLeft:"auto",marginRight:"auto"}}/>
          <h2 style={{fontSize:"23px",textAlign:"center",color:"#5bc5c5"}}>Love</h2>
          <p style={{fontSize:"15px",textAlign:"center",color:"#5bc5c5"}}>Melalui cinta, kebaikan, dan apresiasi kami berkomitmen untuk mengajak perempuan mencintai diri dan membuat kecantikan sejati bersinar.</p>
        </Col>
      </Row>
    </Container>
    </div>
    );
}

function Title(){
  return(
    <div className="hempat" style={{marginTop:"40px"}}>
     <Row>
     <Col><hr/></Col>
     <Col><h4  style={{textAlign:"center",color:"#5bc5c5"}}>
       WHAT'S NEW
      </h4></Col>
     <Col><hr/></Col>
      
      </Row>
    </div>
    );
}

function Product(){
  const Fitur = (props) =>{
    return(
      <Card style={{ width: '10rem' }}>
        <Card.Img variant="top" style={{width:"198px",height:"198px"}} src={props.image} />
        <Card.Body>
          <Card.Title>{props.title}</Card.Title>
          <Card.Text>
          {props.subtitle}
          </Card.Text>
        </Card.Body>
      </Card>
      );
  }

  return(
  <Container style={{marginTop:"50px",marginBottom:"75px"}}>
        <Row>
          <Col>
            <Fitur
              image="https://www.wardahbeauty.com/medias/products/thumbnail-1610430879.png"
              title="BODY SERIES"
              subtitle="Wardah Nature Daily Aloe Hydramild Hand Wash 250ml"
            />
          </Col>
          <Col>
            <Fitur
              image="https://www.wardahbeauty.com/medias/products/thumbnail-1601274402.png"
              title="MOISTURIZER"
              subtitle=" Hydra Rose Moisture Rich Night Gel"
            />
          </Col>
          <Col>
            <Fitur
              image="https://www.wardahbeauty.com/medias/products/thumbnail-1571216396.png"
              title="MOISTURIZER"
              subtitle="Wardah Acnederm Acne Spot Treatment gel"
            />
          </Col>
          <Col>
            <Fitur
              image="https://www.wardahbeauty.com/medias/products/thumbnail-1571054424.png"
              title="LIP CREAM"
              subtitle="Exlusive Matte Lip Cream"
            />
          </Col>
        </Row>
  </Container>
    );
}

function Card1(){
  return(
    <Container style={{marginLeft:"200px"}}>
      <Row>
        <Col>
          <Image src="https://www.wardahbeauty.com/medias/articles/thumb-square-1606456157.png" height="700" width="682" fluid />
        </Col>
        <Col>
          <h4 style={{fontSize:"32px",color:"#5bc5c5"}}>Skincare dengan Kandungan Rose Oil untuk Kulit Lembab yang Glowing</h4>
          <p style={{color:"#5bc5c5",fontSize:"18px"}}>Baru! Wardah Hydra Rose Series untuk Menghidrasi Kulit Cantikmu</p>        
          <hr/>
          <p style={{fontSize:"17px"}}>Kulit yang kering akan menyebabkan wajah jadi gatal, kemerahan, pecah-pecah, tertarik, hingga terlihat kusam. Kalau kamu memiliki masalah kulit kering seperti ini, kamu bisa mulai merangkai skincare regimen dengan produk-produk yang secara spesifik membuat kulit lembab dan lebih glowing. Untuk menge...</p>
          <Button href="#" style={{backgroundColor:"#5bc5c5"}}>SELEBIHNYA</Button>
        </Col>
      </Row>
    </Container>
  );
}




function Card2(){
  return(
    <Container style={{marginLeft:"200spx",marginTop:"-25px"}}>
      <Row>
        <Col>
          <Image src="https://www.wardahbeauty.com/medias/articles/thumb-wide-1596768635.png" height="280" width="300" bsPrefix />
        </Col>
        <Col>
          <h4 style={{fontSize:"17px",color:"#5bc5c5"}}>Skincare dengan Kandungan Rose Oil untuk Kulit Lembab yang Glowing</h4>
          <p style={{color:"#5bc5c5",fontSize:"12px"}}>Baru! Wardah Hydra Rose Series untuk Menghidrasi Kulit Cantikmu</p>        
          <hr/>
          <p style={{fontSize:"10px"}}>Kulit yang kering akan menyebabkan wajah jadi gatal, kemerahan, pecah-pecah, tertarik, hingga terlihat kusam. Kalau kamu memiliki masalah kulit kering seperti ini, kamu bisa mulai merangkai skincare regimen dengan produk-produk yang secara spesifik membuat kulit lembab dan lebih glowing. Untuk menge...</p>
          <a href="#" style={{color:"#5bc5c5",fontSize:"12px",cssFloat:"right"}}>SELEBIHNYA</a>
        </Col>
        <Col>
          <Image src="https://www.wardahbeauty.com/medias/articles/thumb-wide-1586410395.png" height="280" width="300" bsPrefix />
        </Col>
        <Col>
          <h4 style={{fontSize:"17px",color:"#5bc5c5"}}>Skincare dengan Kandungan Rose Oil untuk Kulit Lembab yang Glowing</h4>
          <p style={{color:"#5bc5c5",fontSize:"12px"}}>Baru! Wardah Hydra Rose Series untuk Menghidrasi Kulit Cantikmu</p>        
          <hr/>
          <p style={{fontSize:"10px"}}>Kulit yang kering akan menyebabkan wajah jadi gatal, kemerahan, pecah-pecah, tertarik, hingga terlihat kusam. Kalau kamu memiliki masalah kulit kering seperti ini, kamu bisa mulai merangkai skincare regimen dengan produk-produk yang secara spesifik membuat kulit lembab dan lebih glowing. Untuk menge...</p>
          <a href="#" style={{color:"#5bc5c5",fontSize:"12px",cssFloat:"right"}}>SELEBIHNYA</a>
        </Col>

      </Row>
    </Container>
  );
}



function Bannertiga(){
  return(
      <Image src="https://www.wardahbeauty.com/assets/images/footer-banner-desktop-1600153941.jpg" bsPrefix style={{width:"100%",marginTop:"25px"}} />
    );
}

function Footer(){
  return(
  <div style={{backgroundColor:"#5bc5c5",height:"300px"}}>
    <Row>
      <Col>
        <ul>
          <li style={{listStyleType:"none",fontSize:"15px",fontWeight:"bold",paddingTop:"70px"}}><h8 style={{color:"white"}}>PRODUK</h8></li>
          <li style={{listStyleType:"none",fontSize:"15px"}}><a href="#" style={{color:"#fff"}}>SKINCARE</a></li>
          <li style={{listStyleType:"none",fontSize:"15px"}}><a href="#" style={{color:"#fff"}}>MAKE UP</a></li>
          <li style={{listStyleType:"none",fontSize:"15px"}}><a href="#" style={{color:"#fff"}}>HAIRCARE</a></li>
          <li style={{listStyleType:"none",fontSize:"15px"}}><a href="#" style={{color:"#fff"}}>BODYCARE</a></li>
          <li style={{listStyleType:"none",fontSize:"15px"}}><a href="#" style={{color:"#fff"}}>INSTAPERFECT</a></li>
          <li style={{listStyleType:"none",fontSize:"15px"}}><a href="#" style={{color:"#fff"}}>CRYSTALLURE</a></li>
        </ul>
      </Col> 
      <Col>
        <ul>
          <li style={{listStyleType:"none",fontSize:"15px",fontWeight:"bold",paddingTop:"70px"}}><h8 style={{color:"white"}}>ACTIVITIES</h8></li>
          <li style={{listStyleType:"none",fontSize:"15px"}}><a href="#" style={{color:"#fff"}}>Wardah Inspiring Movement</a></li>
          <li style={{listStyleType:"none",fontSize:"15px"}}><a href="#" style={{color:"#fff"}}>Wardah Fashion Jurney</a></li>
        </ul>
      </Col>   
      <Col>
        <ul>
         <li style={{listStyleType:"none",fontSize:"15px",fontWeight:"lighter",paddingTop:"70px"}}><h8 style={{color:"white"}}>TERHUBUNG DENGAN KAMI</h8></li>
        </ul>
      </Col>   
      <Col>
        <ul>
         <li style={{listStyleType:"none",fontSize:"15px",fontWeight:"lighter",paddingTop:"70px"}}><h8 style={{color:"white"}}>IKUTI BERITA DAN UPDATE KAMI</h8></li>
         <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Control  size="sm" type="email" placeholder="Enter email" style={{backgroundColor:"#5bc5c5",width:"150px"}} />
              <Button size="sm" type="submit" style={{backgroundColor:"#5bc5c5",color:"white",marginTop:"5px"}}>
                Kirim
              </Button>
            </Form.Group>
          </Form>

         <li style={{listStyleType:"none",fontSize:"15px"}}><p style={{color:"#fff"}}>Hak Cipta © Wardah 2021</p></li>
        </ul>
      </Col>
    </Row>
  </div>
  );
}


function App() {
  return (
    <div>
      <Header/>
      <Banner/>
      <Bannerdua/>
      <gambar/>
      <Title/>
      <Product/>
      <Card1/>
      <Card2/>
      <Bannertiga/>
      <Footer/>
    </div>

  );
}

export default App;
